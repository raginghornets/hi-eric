<!!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/images/blueeyeswhitedragon.png"> 
  <title>Home | hi-eric</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <style>
    /* Jumbotron */

    body {
      padding-top: 3.5rem;
    }
  </style>
  <link href="/css/style.min.css" rel="stylesheet">
</head>

<body id="Home">
  <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand page-scroll slide-down" href="#Home">hi-eric</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse slide-down" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link page-scroll" href="#Websites">Websites</a>
        </li>
        <li class="nav-item">
          <a class="nav-link page-scroll" href="#Dance">Dance</a>
        </li>
        <li class="nav-item">
          <a class="nav-link page-scroll" href="#Experience">Experience</a>
        </li>
        <li class="nav-item">
          <a class="nav-link page-scroll" href="#Tools">Tools</a>
        </li>
      </ul>
    </div>
  </nav>
  <main role="main">
    <article>
      <header class="pep-rally">
        <div class="container">
          <div class="row">
            <div class="col-md-6 header-text slide-up">
              <h1 class="display-3">Hello, world!</h1>
              <p>
               Welcome to my website.
              </p>
            </div>
          </div>

          <!-- Arrow scroll down -->
          <div class="row pt-15">
            <div class="col-md-12 text-center">
              <a href="#MyStuff" class="page-scroll slide-up">
                <i class="down"></i>
              </a>
            </div>
          </div>
        </div>
      </header>

      <!-- Accomplishments (Websites and Dance Videos) -->
      <section id="MyStuff">
        <div class="container" id="yearAccordion">
          <?php
          $servername = "";
          $username = "";
          $password = "";
          $dbname = "";
          $conn = new mysqli($servername, $username, $password, $dbname);
          if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
          }
          function echoWebsites($content, $year, $month) {
            $result = $GLOBALS['conn']->query("SELECT * FROM Websites");
            if ($result->num_rows > 0) {
              // Start month
              echoMonth($content, $year, $month);
              // Display cards in month
              while($row = $result->fetch_assoc()) {
                if($row["Content"] == $content && $row["Year"] == $year && $row["Month"] == $month) {
                  // =====Start card=====
                  echo "
              <!-- " . $row["Name"] . " -->
              <div class='card'>
                <div class='card-body'>
                  <a href='" . $row["URL"] . "' target='_blank'>
                    <h4 class='card-title'>" . $row["Name"] . "</h4>
                  </a>
                  <p class='card-text'>" . $row["Description"] . "</p>
                </div>
                      ";
                  // For cards with footer notes
                  if ($row["Note"] != "") {
                      echo "
                <div class='card-footer'>
                  <small class='text-muted'> Note: " . $row["Note"] . "</small>
                </div>
                      ";
                  }
                  echo "
              </div>
                      ";
                  // =====End card=====
                } 
              }
              endMonth($content, $year, $month);
              // End month
            }
          }
          function echoMonth($content, $year, $month) {
              echo "
          <!-- " . $content . $year . $month . " Card -->
          <div class='card'>

            <!-- " . $content . $year . $month . " Header -->
            <a class='card-month' data-parent='#" . $content . $year . "' data-toggle='collapse' href='#" . $content . $year . $month . "'>
              <div class='card-header grayscale-link' style='background: no-repeat center url(/images/" . /*$month .*/".svg)'>
                <h4 class='card-title'>" . $month . "</h4>
              </div>
            </a>

            <!-- " . $content . $year . $month . " Collapse -->
            <div class='p-3 collapse' aria-expanded='false' id='" . $content . $year . $month . "'>
              ";
          }
          function endMonth($content, $year, $month) {
              echo "
            </div>
            <!-- /" . $content . $year . $month . " Collapse -->
          </div>
          <!-- /" . $content . $year . $month . " Card -->
              ";
          }
          function echoYear($content, $year) {
              echo "
      <!-- " . $content . $year . " Card -->
      <div class='card card-year mb-1'>
      
        <!-- " . $content . $year . " Header -->
        <a href='#" . $content . $year . "' data-parent='#yearAccordion' data-toggle='collapse'>
          <div class='card-header card-year-content grayscale-link' style='background: no-repeat center url(/images/" . $content . $year . ".svg)'>
            <div class='card-title'>
              <div class='row'>
                <div class='col-md-6'>
                  <h3 class='display-4 card-title'>" . $year . "</h3>
                </div>
              </div>
            </div>
          </div>
        </a>

        <!-- " . $content . $year . " Collapse -->
        <div id='" . $content . $year . "' class='p-3 collapse'>
              ";
          }

          function endYear($content, $year) {
              echo "
        </div>
        <!-- /" . $content . $year . " Collapse -->
      </div>
      <!-- /" . $content . $year . " Card -->
              ";
          }
          ?>
            <!-- Websites -->
            <h2 id="Websites" class="display-4 category pb-3">My Websites</h2>

            <?php
          echoYear("Website", 2015);
            echoWebsites("Website", 2015, "October");
          endYear("Website", 2015);

          echoYear("Website", 2017);
            echoWebsites("Website", 2017, "September");
            echoWebsites("Website", 2017, "November");
            echoWebsites("Website", 2017, "December");
          endYear("Website", 2017);

          echoYear("Website", 2018);
            echoWebsites("Website", 2018, "January");
          endYear("Website", 2018);
          ?>

              <!-- Dance Videos -->
              <h2 id="Dance" class="display-4 category pb-3">
                My Dance Videos
              </h2>
              <?php
          echoYear("Dance", 2011);
            echoWebsites("Dance", 2011, "December");
          endYear("Dance", 2011);

          echoYear("Dance", 2015);
            echoWebsites("Dance", 2015, "October");
            echoWebsites("Dance", 2015, "November");
            echoWebsites("Dance", 2015, "December");
          endYear("Dance", 2015);

          echoYear("Dance", 2016);
            echoWebsites("Dance", 2016, "January");
            echoWebsites("Dance", 2016, "February");
            echoWebsites("Dance", 2016, "March");
            echoWebsites("Dance", 2016, "April");
            echoWebsites("Dance", 2016, "May");
            echoWebsites("Dance", 2016, "June");
            echoWebsites("Dance", 2016, "October");
            echoWebsites("Dance", 2016, "December");         
          endYear("Dance", 2016);

          echoYear("Dance", 2017);
            echoWebsites("Dance", 2017, "January");
            echoWebsites("Dance", 2017, "March");
            echoWebsites("Dance", 2017, "April");
            echoWebsites("Dance", 2017, "May");
            echoWebsites("Dance", 2017, "October");
            echoWebsites("Dance", 2017, "December");
          endYear("Dance", 2017);

          echoYear("Dance", 2018);
            echoWebsites("Dance", 2018, "March");
          endYear("Dance", 2018);
          ?>
        </div>
      </section>
      <!-- /Accomplishments (Websites and Dance Videos) -->

      <!-- My Experience -->
      <section id="Experience" class="pt-10">
        <div class="container">
          <div class="row mb-3">
            <div class="col-md-12 text-center">
              <h2 class="display-4">My Experience</h2>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-md-12 text-center">
              <p class="btn btn-primary">
                More experience
              </p>
              <p class="btn btn-secondary">
                Less experience
              </p>
            </div>
          </div>
          <div class="row">
            <div class="card-group">
              <?php
              function echoSkillCard($category) {
                $result = $GLOBALS['conn']->query("SELECT * FROM Skills");
                if ($result->num_rows > 0) {
                  echo "
              <div class='card experience-category'>
                <div class='card-body'>
                  <div class='card-block'>
                    <h4 class='card-title'>".$category."</h4>";
                  while($row = $result->fetch_assoc()) {
                    if($row["Category"] == $category) {
                      echo "
                    <p class='d-inline-block'>
                      <a target='_blank' href='" . $row["URL"] . "' class='btn btn-" . $row["Experience"] . "'>
                        " . $row["Name"] . "
                      </a>
                    </p>";
                    }
                  }
                  echo "
                  </div>
                </div>
              </div>";
                }
              }
              echoSkillCard("Languages/Frameworks");
              echoSkillCard("Software");
              echoSkillCard("General");
              ?>
            </div>
          </div>
        </div>
      </section>
      <!-- /My Experience -->

      <!-- I made this website using -->
      <section id="Tools" class="pt-10">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-12">
              <h2 class="display-4">I made this website using</h2>
            </div>
          </div>
          <div class="card-group">
            <?php 
            function echoMadeWithCard($img, $alt, $title, $url, $a) {
              echo "
        <div class='card border-0'>
          <div class='card-body'>
            <img class='card-img-top mb-3' src='".$img."' alt='".$alt."'>
            <div class='card-block text-center'>
              <h4 class='card-title'>".$title."</h4>
              <a target='_blank' href='".$url."'>".$a."</a>
            </div>
          </div>
        </div>";
            }
            echoMadeWithCard("/images/VSCode.svg", "Visual Studio Code Image", "Visual Studio Code", "https://code.visualstudio.com/", "Learn more about VSCode");
            echoMadeWithCard("/images/Bootstrap.svg","Bootstrap Image","Bootstrap","https://getbootstrap.com/","Learn more about Bootstrap");
            echoMadeWithCard("/images/000webhost.svg","000webhost Image","000webhost","https://www.000webhost.com/","Learn more about 000webhost");
            echoMadeWithCard("/images/FileZilla.svg","FileZilla Image","FileZilla","https://filezilla-project.org/","Learn more about FileZilla");
            echoMadeWithCard("/images/phpMyAdmin.svg","phpMyAdmin Image","phpMyAdmin","https://www.phpmyadmin.net/","Learn more about phpMyAdmin");
            ?>
          </div>
        </div>
      </section>
      <!-- /I made this website using -->
    </article>
  </main>
  <footer class="container pt-10">
    <hr>
    <div class="row">
      <div class="col-md-12 mb-5">
        &copy; 2017-2018
        <div class="float-right">
          <a href="https://github.com/airicbear" target="_blank">
            <img width="30rem" src="/images/Github.svg" alt="GithubIcon">
          </a>
        </div>
      </div>
    </div>
  </footer>

  <div class="webhost-bar"></div>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="/js/bootstrap.min.js"></script>
  <!-- Smooth Scroll -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="/js/smoothscroll.js"></script>
  <!-- Close connection with database -->
  <?php $conn->close(); ?>
</body>

</html>