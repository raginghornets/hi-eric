// Bootstrap
let bootstrap = document.createElement('link');
bootstrap.rel = 'stylesheet';
bootstrap.href = '/css/bootstrap.min.css';
document.head.appendChild(bootstrap);

// Container
let marbleContainer = document.createElement('div');
marbleContainer.className = 'container';
document.body.appendChild(marbleContainer);

  // Row
  let marbleContainerRow = document.createElement('div');
  marbleContainerRow.className = 'row';
  marbleContainer.appendChild(marbleContainerRow);

    // Column
    let marbleContainerColumn = document.createElement('div');
    marbleContainerColumn.className = 'col-md-12';
    marbleContainerRow.appendChild(marbleContainerColumn);

      // Card
      let marbleContainerCard = document.createElement('div');
      marbleContainerCard.className = 'card';
      marbleContainerColumn.appendChild(marbleContainerCard);

        // Card header
        let marbleContainerCardHeader = document.createElement('div');
        marbleContainerCardHeader.className = 'card-header';
        marbleContainerCardHeader.style.backgroundColor = '#353535';
        marbleContainerCardHeader.style.color = 'white';
        marbleContainerCard.appendChild(marbleContainerCardHeader);

          // Card header text
          let marbleContainerCardHeaderText = document.createElement('h1');
          marbleContainerCardHeaderText.className = 'display-4';
          marbleContainerCardHeaderText.innerHTML = 'Probability';
          marbleContainerCardHeader.appendChild(marbleContainerCardHeaderText);

        // Card body
        let marbleContainerCardBody = document.createElement('div');
        marbleContainerCardBody.className = 'card-body';
        marbleContainerCard.appendChild(marbleContainerCardBody);

          // Form
          let myForm = document.createElement('form');
          myForm.className = 'form-inline';
          myForm.id = 'myForm';
          marbleContainerCardBody.appendChild(myForm);

            // Prevent myForm submission
            myForm.addEventListener('submit', function(event){
              event.preventDefault()
              marbleAmount.value = '';
              marbleColor.focus();
            });

            // Marble color input
            let marbleColorInput = document.createElement('select');
            marbleColorInput.className = 'form-control mb-2 mr-sm-2 mb-sm-0';
            marbleColorInput.id = 'marbleColor';
            myForm.appendChild(marbleColorInput);

              let setOption = function(letiable, val, text) {
                letiable = document.createElement('option');
                letiable.value = val;
                letiable.innerHTML = val;
                marbleColorInput.appendChild(letiable);
              };
              // Option - red
              let marbleRed;
              setOption(marbleRed, 'red');

              // Option - orange
              let marbleOrange;
              setOption(marbleOrange, 'orange');

              // Option - yellow
              let marbleYellow;
              setOption(marbleYellow, 'yellow');

              // Option - green
              let marbleGreen;
              setOption(marbleGreen, 'green');

              // Option - blue
              let marbleBlue;
              setOption(marbleBlue, 'blue');

              // Option - indigo
              let marbleIndigo;
              setOption(marbleIndigo, 'indigo');

              // Option - violet
              let marbleViolet;
              setOption(marbleViolet, 'violet');
            
            // Marble amount input 
            let marbleAmountInput = document.createElement('input');
            marbleAmountInput.className = 'form-control mb-2 mr-sm-2 mb-sm-0';
            marbleAmountInput.id = 'marbleAmount';
            marbleAmountInput.type = 'number';
            myForm.appendChild(marbleAmountInput);
          
          // Marble button
          let marbleButton = document.createElement('button');
          marbleButton.className = 'btn btn-primary';
          marbleButton.setAttribute('onclick','addMarble(new Marble(marbleColorInput.value, marbleAmountInput.value)); updateMarbles();');
          marbleButton.innerHTML = 'Submit';
          myForm.appendChild(marbleButton);

          // Marble list
          let marbleList = document.createElement('div');
          marbleList.className = 'col-md-12 card-group';
          marbleList.id = 'marbleList';
          marbleContainerCardBody.appendChild(marbleList);

let marbles = [];

function Marble(marbleColor, marbleAmount) {
  this.marbleColor = marbleColor;
  this.marbleAmount = marbleAmount;
}

let updateMarbles = function () {
  let i;
  for (i = 0; i < marbles.length; i++) {
    let mProb = document.getElementsByClassName('marbleProbability')[i];
    mProb.innerHTML = marbleProbability(marbles[i]) + '%';
    let mAmt = document.getElementsByClassName('marble')[i];
    mAmt.innerHTML = marbles[i].marbleAmount;
  }
}

let addMarble = function (marble) {
  let i;
  for (i = 0; i < marbles.length; i++) {
    let mProb = document.getElementsByClassName('marbleProbability')[i];
    let mAmt = document.getElementsByClassName('marble')[i];
    // Check for duplicate marble color
    if (marbles[i].marbleColor === marble.marbleColor) {
      // Replace marble amount
      if (marbles[i].marbleAmount !== marble.marbleAmount && marble.marbleAmount !== '') {
        marbles[i].marbleAmount = marble.marbleAmount;
        mProb.innerHTML = marbleProbability(marbles[i]) + '%';
        mAmt.innerHTML = marble.marbleAmount;
      }
      return;
    }
  }

  if(marble.marbleColor == '' || marble.marbleAmount == '') return;
  marbles.push(marble);

  // New card
  let card = document.createElement('div');
  card.className = 'card text-center marbleCard';
  card.style.backgroundColor = marble.marbleColor;
  card.style.color = 'white'
  card.style.marginTop = '1rem';
  card.style.overflow = "hidden";
  card.style.textShadow = '0px 0px 1px black';
  card.style.transition = 'all 0.25s ease';
  card.style.width = '70rem';
  
  card.addEventListener('click', function(event){
    card.remove();
    marbles.pop(marble);
  });

  card.addEventListener('mouseover', function(event){
    card.style.cursor = 'pointer';
    card.style.filter = 'grayscale(60%)';
    cardDeleteContainer.style.display = 'block';
    cardTextContainer.style.visibility = 'hidden';
  });

  card.addEventListener('mouseout', function(event){
    card.style.filter = 'grayscale(0%)';
    cardDeleteContainer.style.display = 'none';
    cardTextContainer.style.visibility = 'visible';
  });

  // New card body
  let cardBody = document.createElement('div');
  cardBody.className = 'card-body';

  // Text container
  let cardTextContainer = document.createElement('div');
  cardTextContainer.className = "cardTextContainer";

  // New card text
  let cardText = document.createElement('p');
  cardText.className = 'marble';
  cardText.innerHTML = marble.marbleAmount;

  // Probability
  let probability = document.createElement('p');
  probability.className = 'marbleProbability';
  probability.innerHTML = marbleProbability(marble) + "%";

  // New card delete container
  let cardDeleteContainer = document.createElement('div');
  cardDeleteContainer.style.marginTop = '-4.5rem';  
  cardDeleteContainer.style.paddingBottom = "0.4rem";
  cardDeleteContainer.style.display = 'none';
  cardDeleteContainer.style.transition = 'all 0.5s ease';

  // New card delete text
  let cardDeleteText = document.createElement('h1');
  cardDeleteText.style.fontSize = "3rem";
  cardDeleteText.innerHTML = 'Delete';

  // Add to HTML
  marbleList.appendChild(card);
  card.appendChild(cardBody);
  cardBody.appendChild(cardTextContainer);
  cardTextContainer.appendChild(cardText);
  cardTextContainer.appendChild(probability);
  cardBody.appendChild(cardDeleteContainer);
  cardDeleteContainer.appendChild(cardDeleteText);
}

let total = function () {
  let result = 0;
  let i;
  for(i = 0; i < marbles.length; i++) {
    result += parseFloat(marbles[i].marbleAmount);
  }
  return result;
}

let marbleProbability = function (marble) {
  return 100*(marble.marbleAmount / total());
}