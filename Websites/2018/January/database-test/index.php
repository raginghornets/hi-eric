<html>

<head>
    <title>Database | Eric's Website</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="icon" href="/images/cat-icon.ico">
</head>

<body>
    <?php
  $servername = "";
  $username = "";
  $password = "";
  $dbname = "";
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
      die("Connection failed: ".$conn->connect_error);
  } 
  $sql = "SELECT * FROM Websites";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
  echo "<table class='table table-responsive'>
    <tr>
      <th>ID</th>
      <th>Content</th>
      <th>Name</th>
      <th>Description</th>
      <th>Month</th>
      <th>Year</th>
      <th>URL</th>
      <th>Note</th>
    </tr>";
        while($row = $result->fetch_assoc()) {
            echo "
    <tr>
      <td>" 
          .$row["ID"].
      "</td>
      <td>"
          .$row["Content"].
      "</td>
      <td>"
          .$row["Name"].
          "</td>
      <td>"
          .$row["Description"].
      "</td>
      <td>"
          .$row["Month"].
      "</td>
      <td>"
          .$row["Year"].
      "</td>
      <td>
        <a href=".$row["URL"]." target='_blank'>"
            .$row["URL"]. 
        "</a>
      </td>";
        if($row["Note"] != "") {
        echo "
      <td>"
          .$row["Note"]. 
      "</td>";
        }
        echo "
    </tr>";
      }
      echo "
</table>";
  } else {
      echo "0 results";
  }
  $conn->close();
  ?>
</body>

</html>