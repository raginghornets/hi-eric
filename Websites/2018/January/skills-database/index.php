<html>

<head>
  <title>Database | Eric's Website</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="icon" href="/images/cat-icon.ico">
</head>

<body>
  <?php
    $servername = "";
    $username = "";
    $password = "";
    $dbname = "";
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: ".$conn->connect_error);
    } 
    $sql = "SELECT * FROM Skills";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        echo "<table class='table table-responsive'>
    <tr>
      <th>ID</th>
      <th>Category</th>
      <th>Name</th>
      <th>Experience</th>
      <th>URL</th>
    </tr>";
        while($row = $result->fetch_assoc()) {
            echo "
    <tr>
      <td>" 
          .$row["ID"].
      "</td>
      <td>"
          .$row["Category"].
      "</td>
      <td>"
          .$row["Name"].
          "</td>
      <td>"
          .$row["Experience"].
      "</td>
      <td>
        <a href=".$row["URL"]." target='_blank'>"
            .$row["URL"].
        "</a>
      </td>
    </tr>";
        }
echo "
</table>";
    } else {
        echo "0 results";
    }
    $conn->close();
    ?>
</body>

</html>