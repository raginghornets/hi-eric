-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2019 at 06:26 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id4080927_databasetest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Websites`
--

CREATE TABLE `Websites` (
  `ID` smallint(6) NOT NULL,
  `Content` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `Name` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` tinytext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Month` char(9) COLLATE utf8_unicode_ci NOT NULL,
  `Year` year(4) NOT NULL,
  `URL` varchar(2083) COLLATE utf8_unicode_ci NOT NULL,
  `Note` tinytext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Websites`
--

INSERT INTO `Websites` (`ID`, `Content`, `Name`, `Description`, `Month`, `Year`, `URL`, `Note`) VALUES
(1, 'Website', 'Throwaway 9/17/2017', 'Doge', 'September', 2017, '/Websites/2017/September/throwaway09172017', 'The brick game isn\'t mine.'),
(2, 'Website', 'Game Website', 'Fixed header and footer test', 'September', 2017, '/Websites/2017/September/gamewebsite', ''),
(3, 'Website', 'New Website Yay', 'Scrollbar styling test', 'September', 2017, '/Websites/2017/September/newwebsiteyay', ''),
(4, 'Website', 'Picture Collection', 'Gradient & Animation Test', 'November', 2017, '/Websites/2017/November/PictureCollection', ''),
(5, 'Website', 'Hello World', 'GitHub Test', 'November', 2017, '/Websites/2017/November/HelloWorld', ''),
(6, 'Website', 'HTML5 Game', 'Javascript Game Test', 'December', 2017, '/Websites/2017/December/html5game', 'Unfortunately, I do not remember the guide I used to create this.'),
(7, 'Website', 'Snake Game', 'Attempt at making the snake game', 'December', 2017, '/Websites/2017/December/snakegame/', 'This is really bad.'),
(8, 'Website', 'Berwyn Nails', 'Attempt at making a website for Berwyn Nails', 'December', 2017, '/Websites/2017/December/berwynnails/', ''),
(9, 'Website', 'Throwaway 12/12/2017', 'Move a square - Javascript', 'December', 2017, '/Websites/2017/December/throwaway12122017/', ''),
(10, 'Website', 'Throwaway 12/13/2017', 'It is almost 2018', 'December', 2017, '/Websites/2017/December/throwaway12132017/', ''),
(11, 'Website', 'Throwaway 12/17/2017', 'Donald Trump and Ajit Pai', 'December', 2017, '/Websites/2017/December/throwaway12172017/', ''),
(12, 'Website', 'Throwaway 12/18/2017', 'Trash', 'December', 2017, '/Websites/2017/December/throwaway12182017/', ''),
(13, 'Website', 'Hi', 'Hi, my name is Eric', 'December', 2017, '/Websites/2017/December/hi', ''),
(14, 'Website', 'Bookmark', 'Traversy Media\'s bookmark tutorial', 'December', 2017, '/Websites/2017/December/bookmark', ''),
(15, 'Website', 'Database Test', 'Database test', 'January', 2018, '/Websites/2018/January/database-test/', ''),
(16, 'Dance', 'Raise Your Weapon [Noisia Remix]', 'December 13, 2011', 'December', 2011, 'https://youtu.be/fMDvZ6bdqZE', ''),
(17, 'Dance', 'Crave You Dubstep', 'December 18, 2011', 'December', 2011, 'https://youtu.be/hSl2SJj-rDs', ''),
(18, 'Dance', 'Stoga Breakers | 10/6/2015 | Club Session', 'October 6, 2015', 'October', 2015, 'https://youtu.be/rEXOaphmm_A', ''),
(19, 'Dance', 'Club Ending Cypher', 'October 14, 2015', 'October', 2015, 'https://youtu.be/sBVkk6czsso', ''),
(20, 'Dance', 'Stoga BC One 10/21/2015', 'October 21, 2015', 'October', 2015, 'https://www.youtube.com/watch?v=aQshT9RWYFg&list=PLV7tF5mNFFQDIaTVj0mSQE_2lOnrmPsKt', ''),
(21, 'Dance', 'EPIC Stoga Breakers TEAM BATTLE', 'October 28, 2015', 'October', 2015, 'https://youtu.be/JSQaxhLWOGM', ''),
(22, 'Dance', 'Cold Ankle Crew vs Long Sock Rockers', 'November 17, 2015', 'November', 2015, 'https://youtu.be/NnlHoZmPK0c', ''),
(23, 'Dance', 'Black Foot Breakers X All Inclusive Individuals', 'November 18, 2015', 'November', 2015, 'https://youtu.be/wIa-M3v2AiY', ''),
(24, 'Dance', 'Stoga Breakers | 11/17/15 | Club Session', 'November 20, 2015', 'November', 2015, 'https://youtu.be/b3-47Dnc2TQ', ''),
(25, 'Dance', 'Conestoga Cornucopia 2015', 'November 30, 2015', 'November', 2015, 'https://youtu.be/MTaskgl5rsU', ''),
(26, 'Dance', 'Multicultural Event | 12/3/15 | Stoga Breakers', 'December 3, 2015', 'December', 2015, 'https://youtu.be/1BtPzBYzPFA', ''),
(27, 'Dance', 'Stoga Breakers | YMCA | Session', 'December 10, 2015', 'December', 2015, 'https://youtu.be/DlQMVZdfs9Q', ''),
(28, 'Dance', 'Stoga Breakers | 12/15/15 | Stoga v Ridley Girls Basketball Halftime Show', 'December 15, 2015', 'December', 2015, 'https://youtu.be/xL9dhrIfnmU', ''),
(29, 'Dance', 'December 23, 2015', 'December 23, 2015', 'December', 2015, 'https://youtu.be/60QORqtWAzU', ''),
(30, 'Dance', 'Eric\'s 2015 Trailer', 'January 8, 2016', 'January', 2016, 'https://youtu.be/ROZiVK4r3F4', ''),
(31, 'Dance', 'Quasi-Round Robin', 'February 4, 2016', 'February', 2016, 'https://www.youtube.com/watch?v=zcFGodF_kr4&list=PLV7tF5mNFFQAw11qq7A0SmaQaunYEcWMW', ''),
(32, 'Dance', 'Round Robin 3/1', 'March 1, 2016', 'March', 2016, 'https://www.youtube.com/watch?v=_KovCODtgGI&list=PLV7tF5mNFFQBac2KgOdiV2JzZuUPmD1x6', ''),
(33, 'Dance', 'Pokemon battles 3/2/16', 'March 2, 2016', 'March', 2016, 'https://www.youtube.com/watch?v=vdFzywVEf2o&list=PLV7tF5mNFFQBh1wMSQ9jl7btTFn6DNdmw', ''),
(34, 'Dance', 'Stoga Breakers Round Robin Tournament', 'March 7, 2016', 'March', 2016, 'https://youtu.be/WJAoK_648x0', ''),
(35, 'Dance', 'Round Robin Final | Team 2 vs Team 5', 'March 10, 2016', 'March', 2016, 'https://youtu.be/r5lb7hZTwbc', ''),
(36, 'Dance', 'T/E\'s Got Talent | Stoga Breakers', 'March 31, 2016', 'March', 2016, 'https://youtu.be/5DaQAC6ijVY', ''),
(37, 'Dance', 'Mini-THON | Stoga Breakers Performance', 'April 6, 2016', 'April', 2016, 'https://youtu.be/84NiXK60Hj8', ''),
(38, 'Dance', 'Best Buddies Performance | Stoga Breakers', 'April 15, 2016', 'April', 2016, 'https://youtu.be/BLG9N-zYo5o', ''),
(39, 'Dance', 'Diversity Conference Practice', 'April 19, 2016', 'April', 2016, 'https://youtu.be/gnOOBhCLGpY', ''),
(40, 'Dance', 'More Diversity Conference Practice', 'April 21, 2016', 'April', 2016, 'https://youtu.be/SZ59P5g3NQI', ''),
(41, 'Dance', 'Kennett High School Diversity Conference Talent Show | Stoga Breakers', 'April 25, 2016', 'April', 2016, 'https://youtu.be/RcD76lrJGEw', ''),
(42, 'Dance', 'Seniors vs EVERYONE', 'May 4, 2016', 'May', 2016, 'https://youtu.be/P7bKsOX9tlI', ''),
(43, 'Dance', 'Club Day Tourney (5/18/16)', 'May 18, 2016', 'May', 2016, 'https://www.youtube.com/watch?v=3v_hWc2ka70&list=PLV7tF5mNFFQDXS-PfYPYf7R9sWEmHcwFu', ''),
(44, 'Dance', 'Stoga Breakers Tourney | Alumni Day', 'May 19, 2016', 'May', 2016, 'https://youtu.be/TVuFohWiXsU', ''),
(45, 'Dance', 'Small 3v3 Tourney', 'May 31, 2016', 'May', 2016, 'https://www.youtube.com/watch?v=Gd0BfHHGEeU&list=PLV7tF5mNFFQBTet7xZ530UzKgFEVqk7Ni', ''),
(46, 'Dance', 'CLUB Battles 6/1/16', 'June 1, 2016', 'June', 2016, 'https://www.youtube.com/watch?v=CV4bV059OpI&list=PLV7tF5mNFFQDWtH4sRo441Srj-6Q0_lNH', ''),
(47, 'Dance', 'Conestoga Pep Rally \'16 | Stoga Breakers', 'October 4, 2016', 'October', 2016, 'https://youtu.be/Y7rFz9mQ5ns', ''),
(48, 'Dance', 'Ikon Rhythm Ta Dance - Stoga KDC', 'December 10, 2016', 'December', 2016, 'https://youtu.be/jdlDhEso650', ''),
(49, 'Dance', 'Conestoga Junior Cabaret 2017 | Stoga Breakers', 'March 16, 2017', 'March', 2017, 'https://youtu.be/PI75JKnV1BI', ''),
(50, 'Dance', '2017 Mini - Thon \'Good boy\'&\'Hobgoblin\'&\'Bang Bang Bang\' Dance Cover', 'March 31, 2017', 'March', 2017, 'https://youtu.be/esl4or3P_DA', ''),
(51, 'Dance', '2017 Kennett High School Diversity Conference Talent Show', 'April 26, 2017', 'April', 2017, 'https://youtu.be/5m-dPuQdPr0', ''),
(52, 'Dance', '[STOGA KDC] TWICE \"TT\", CLC \"HOBGOBLIN\", BIG BANG \"BANG BANG BANG\" MIX DANCE COVERM', 'May 9, 2017', 'May', 2017, 'https://youtu.be/ezH3ix7Fzl8', ''),
(53, 'Dance', 'Conestoga Pep Rally \'17 (@Teamer Field) | Stoga Breakers', 'October 6, 2017', 'October', 2017, 'https://youtu.be/wuNb30WTJ28', ''),
(54, 'Dance', 'Junior Cabaret- Kpop Club', 'December 20, 2017', 'December', 2017, 'https://youtu.be/6k07vNKHSY4', ''),
(55, 'Website', 'Skills database', 'Skills database', 'January', 2018, '/Websites/2018/January/skills-database', ''),
(56, 'Website', 'JavaScript Test', 'Learning JavaScript from w3schools', 'January', 2018, '/Websites/2018/January/javascript-test', ''),
(57, 'Dance', 'Club Day 1/4/17', 'January 4, 2017', 'January', 2017, 'https://www.youtube.com/watch?v=KIVgOEbN8tk&index=1&list=PLLXIqFUFhzM6aaRJLOZcapn5pM0-4Y6r8', ''),
(58, 'Website', 'Probability', 'Everything JavaScript', 'January', 2018, '/Websites/2018/January/probability', ''),
(59, 'Website', 'Calculator', 'JavaScript Calculator', 'January', 2018, '/Websites/2018/January/calculator', ''),
(60, 'Website', '19nguyene', '19nguyene.hostzi.com', 'October', 2015, '/Websites/2015/October/19nguyene', 'This is really old... and cringeworthy.'),
(61, 'Dance', 'Conestoga High School Mini-Thon 2018 | Stoga Breakers', 'March 17, 2018', 'March', 2018, 'https://youtu.be/QssHyQVT-wc', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Websites`
--
ALTER TABLE `Websites`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
