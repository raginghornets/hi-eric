<!DOCTYPE HTML>
<html class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../images/cat-icon.ico">
  <title>Edit | Eric's Website</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="edit.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <div class="container">
    <h1 id="Website">Add Website</h1>
    <form action="index.php" method="post">
      <div class="container">

        <!-- Add Website - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="ID" required>

        <br><br>

        <!-- Add Website - Content -->
        <label for="Content">
          <b>Content</b>
        </label>
        <input type="text" placeholder="Content" name="Content" required>

        <!-- Add Website - Name -->
        <label for="Name">
          <b>Name</b>
        </label>
        <input type="text" placeholder="Name" name="Name" required>

        <!-- Add Website - Description -->
        <label for="Description">
          <b>Description</b>
        </label>
        <input type="text" placeholder="Description" name="Description">

        <br><br>

        <!-- Add Website - Month -->
        <label for="Month">
          <b>Month</b>
        </label>
        <select name="Month">
          <option value="January" selected>January</option>
          <option value="February">February</option>
          <option value="March">March</option>
          <option value="April">April</option>
          <option value="May">May</option>
          <option value="June">June</option>
          <option value="July">July</option>
          <option value="August">August</option>
          <option value="September">September</option>
          <option value="October">October</option>
          <option value="November">November</option>
          <option value="December">December</option>
        </select>

        <br><br>

        <!-- Add Website - Year -->
        <label for="Year">
          <b>Year</b>
        </label>

        <br>

        <input type="number" placeholder=" Year" name="Year" required>

        <br><br>

        <!-- Add Website - URL -->
        <label for="URL">
          <b>URL</b>
        </label>
        <input type="text" placeholder="URL" name="URL" required>

        <!-- Add Website - Note -->
        <label for="Note">
          <b>Note</b>
        </label>
        <input type="text" placeholder="Note" name="Note">

        <!-- SUBMIT - Add Website -->
        <button type="submit">Submit</button>
      </div>
    </form>

    <br>

    <!-- Modify Website -->
    <h1>Modify Website</h1>
    <form action="index.php" method="post">
      <div class="container">
        <!-- Modify Website - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="id_modify" required>

        <br><br>

        <!-- Modify Website - Content -->
        <label for="Content">
          <b>Content</b>
        </label>
        <input type="text" placeholder="Content" name="content_modify" required>

        <!-- Modify Website - Name -->
        <label for="Name">
          <b>Name</b>
        </label>
        <input type="text" placeholder="Name" name="name_modify" required>

        <!-- Modify Website - Description -->
        <label for="Description">
          <b>Description</b>
        </label>
        <input type="text" placeholder="Description" name="description_modify">

        <br><br>

        <!-- Modify Website - Month -->
        <label for="Month">
          <b>Month</b>
        </label>
        <select name="month_modify">
          <option value="January" selected>January</option>
          <option value="February">February</option>
          <option value="March">March</option>
          <option value="April">April</option>
          <option value="May">May</option>
          <option value="June">June</option>
          <option value="July">July</option>
          <option value="August">August</option>
          <option value="September">September</option>
          <option value="October">October</option>
          <option value="November">November</option>
          <option value="December">December</option>
        </select>

        <br><br>

        <!-- Modify Website - Year -->
        <label for="Year">
          <b>Year</b>
        </label>

        <br>

        <input type="number" placeholder=" Year" name="year_modify" required>
        
        <br><br>

        <!-- Modify Website - URL -->
        <label for="URL">
          <b>URL</b>
        </label>
        <input type="text" placeholder="URL" name="URL_modify" required>

        <!-- Modify Website - Note -->
        <label for="Note">
          <b>Note</b>
        </label>
        <input type="text" placeholder="Note" name="note_modify">

        <!-- SUBMIT - Modify Website -->
        <button type="submit">Submit</button>
      </div>
    </form>

    <br>

    <!-- Delete Website -->
    <h1>Delete Website</h1>
    <form action="index.php" method="post">
      <div class="container">
        <!-- Delete Website - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="id_delete" required>

        <br><br>

        <!-- SUBMIT - Delete Website -->
        <button type="submit">Submit</button>
      </div>
    </form>

    <br><br>

    <!-- Add Skill -->
    <h1 id="Skill">Add Skill</h1>
    <form action="index.php#Skill" method="post">
      <div class="container">

        <!-- Add Skill - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="skill_id" required>

        <br><br>

        <!-- Add Skill - Category -->
        <label for="Category">
          <b>Category</b>
        </label>
        <input type="text" placeholder="Category" name="skill_category" required>

        <!-- Add Skill - Name -->
        <label for="Name">
          <b>Name</b>
        </label>
        <input type="text" placeholder="Name" name="skill_name" required>

        <!-- Add Skill - Experience -->
        <label for="Experience">
          <b>Experience</b>
        </label>
        <input type="text" placeholder="Experience" name="skill_experience" required>

        <!-- Add Skill - URL -->
        <label for="URL">
          <b>URL</b>
        </label>
        <input type="text" placeholder="URL" name="skill_URL" required>

        <br><br>

        <!-- SUBMIT - Add Skill -->
        <button type="submit">Submit</button>
      </div>
    </form>
    <br>
    <h1>Modify Skill</h1>
    <form action="index.php#Skill" method="post">
      <div class="container">

        <!-- Modify Skill - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="skill_id_modify" required>

        <br><br>

        <!-- Modify Skill - Category -->
        <label for="Category">
          <b>Category</b>
        </label>
        <input type="text" placeholder="Category" name="skill_category_modify" required>

        <!-- Modify Skill - Name -->
        <label for="Name">
          <b>Name</b>
        </label>
        <input type="text" placeholder="Name" name="skill_name_modify" required>

        <!-- Modify Skill - Experience -->
        <label for="Experience">
          <b>Experience</b>
        </label>
        <input type="text" placeholder="Experience" name="skill_experience_modify" required>

        <!-- Modify Skill - URL -->
        <label for="URL">
          <b>URL</b>
        </label>
        <input type="text" placeholder="URL" name="skill_URL_modify" required>

        <!-- SUBMIT - Modify Skill -->
        <button type="submit">Submit</button>
      </div>
    </form>
    
    <br>

    <!-- Delete Skill -->
    <h1>Delete Skill</h1>
    <form action="index.php#Skill" method="post">
      <div class="container">
        <!-- Delete Skill - ID -->
        <label for="ID">
          <b>ID</b>
        </label>
        <br>
        <input type="number" placeholder=" ID" name="skill_id_delete" required>

        <br><br>

        <!-- SUBMIT - Delete Skill -->
        <button type="submit">Submit</button>
      </div>
    </form>

    <br>

    <!-- SQL Inject -->
    <h1>SQL</h1>
    <form action="index.php" method="post">
      <div class="container">
        <input type="text" placeholder="Inject SQL" name="sql_inject" required>
        <br>
        <br>
        <button type="submit">Submit</button>
      </div>
    </form>

    <?php 
    $servername = "";
    $username = "";
    $password = "";
    $dbname = "";

    $link = new mysqli($servername, $username, $password, $dbname);

    // Websites
      // Add Website
    if (isset($_POST['ID'])) {
      $id = mysqli_real_escape_string($link, $_POST['ID']);
      $content = mysqli_real_escape_string($link, $_POST['Content']);
      $name = mysqli_real_escape_string($link, $_POST['Name']);
      $description = mysqli_real_escape_string($link, $_POST['Description']);
      $month = mysqli_real_escape_string($link, $_POST['Month']);
      $year = mysqli_real_escape_string($link, $_POST['Year']);
      $URL = mysqli_real_escape_string($link, $_POST['URL']);
      $note = mysqli_real_escape_string($link, $_POST['Note']);
      
      $sql = "INSERT INTO Websites (`ID`, `Content`, `Name`, `Description`, `Month`, `Year`, `URL`, `Note`) 
      VALUES ('$id', '$content','$name','$description','$month','$year','$URL','$note')";

      $result = $link->query($sql);
    }
      // Modify Website
    if (isset($_POST['id_modify'])) {
      $stmt = $link->prepare('UPDATE Websites SET `Content`= ? , `Name`= ? , `Description`= ? , `Month`= ? , `Year`= ? , `URL`= ? , `Note`= ? WHERE id = ?');
      $stmt->bind_param('ssssissi', $content, $name, $description, $month, $year, $URL, $note, $id); // 's' specifies the variable type => 'string'
      
      $content = $_POST['content_modify'];
      $name = $_POST['name_modify'];
      $description = $_POST['description_modify'];
      $month = $_POST['month_modify'];
      $year = $_POST['year_modify'];
      $URL = $_POST['URL_modify'];
      $note = $_POST['note_modify'];
      $id = $_POST['id_modify'];
      $stmt->execute();
    }
      // Delete Website
    if (isset($_POST['id_delete'])) {
      $stmt = $link->prepare('DELETE FROM Websites WHERE id = ?');
      $stmt->bind_param('i', $id); // 'i' specifies the variable type => 'integer'
      $id = $_POST['id_delete'];
      $stmt->execute();
    }

    // Skills
      // Add Skill
    if (isset($_POST['skill_id'])) {
      $id = mysqli_real_escape_string($link, $_POST['skill_id']);
      $category = mysqli_real_escape_string($link, $_POST['skill_category']);
      $name = mysqli_real_escape_string($link, $_POST['skill_name']);
      $experience = mysqli_real_escape_string($link, $_POST['skill_experience']);
      $URL = mysqli_real_escape_string($link, $_POST['skill_URL']);
      
      $sql = "INSERT Skills (`ID`, `Category`, `Name`, `Experience`, `URL`) 
      VALUES ('$id', '$category', '$name', '$experience', '$URL')";

      $result = $link->query($sql);
    }
      // Modify Skill
    if (isset($_POST['skill_id_modify'])) {
      $stmt = $link->prepare('UPDATE Skills SET `Category` = ?, `Name`= ? , `Experience`= ? , `URL`= ? WHERE id = ?');
      $stmt->bind_param('ssssi', $category, $name, $experience, $URL, $id);
      $id = mysqli_real_escape_string($link, $_POST['skill_id_modify']);
      $category = mysqli_real_escape_string($link, $_POST['skill_category_modify']);
      $name = mysqli_real_escape_string($link, $_POST['skill_name_modify']);
      $experience = mysqli_real_escape_string($link, $_POST['skill_experience_modify']);
      $URL = mysqli_real_escape_string($link, $_POST['skill_URL_modify']);
      $stmt->execute();
    }
      // Delete Skill
    if (isset($_POST['skill_id_delete'])) {
      $id = mysqli_real_escape_string($link, $_POST['skill_id_delete']);
      
      $stmt = $link->prepare('DELETE FROM Skills WHERE id = ?');
      $stmt->bind_param('i', $id); // 'i' specifies the variable type => 'integer'
      $id = $_POST['skill_id_delete'];
      $stmt->execute();
    }

    // SQL

    if (isset($_POST['sql_inject'])) {
      $stmt = $link->prepare($_POST['sql_inject']);
      $stmt->execute();
    }

    $link->close();
    ?>
    <br>
  </div>
  <div class="webhost-bar"></div>
</body>

</html>